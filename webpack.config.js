const webpack = require("webpack");
const path = require("path");

module.exports = {
    mode: "development",
    entry: {
        index: path.join(__dirname, "src", "index.js")
    },
    context: path.join(__dirname, "src"),
    output: {
        path: path.join(__dirname, "static"),
        publicPath: "/js/",
        chunkFilename: "[name].chunk.js",
        filename: "[name].bundle.js"
    },
    module: {
        rules: [
            {
                test: /\.js?$/,
                exclude: /(node_modules\/)/,
                loader: "babel-loader",
                options: {
                    cacheDirectory: "babel_cache",
                    presets: ["@babel/react", ["@babel/env", { modules: false, useBuiltIns: "usage" }]],
                    plugins: [
                        ["@babel/plugin-syntax-object-rest-spread"],
                        ["@babel/plugin-syntax-async-generators"],
                        ["@babel/plugin-transform-regenerator"],
                        ["@babel/plugin-proposal-class-properties"]
                    ]
                }
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: "style-loader" // creates style nodes from JS strings
                    },
                    {
                        loader: "css-loader" // translates CSS into CommonJS
                    },
                    {
                        loader: "sass-loader" // compiles Sass to CSS
                    }
                ]
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            "process.env": {
                APP_ENV: JSON.stringify("browser")
            }
        })
    ],
    optimization: {
        splitChunks: {
            name: "commons"
        }
    },
    resolve: {
        modules: ["node_modules"],
        alias: { },
        extensions: [".js", ".jsx"]
    },
    externals: {
        fs: "{}",
        tls: "{}",
        net: "{}",
        console: "{}",
        v8: "{}"
    }
    // node: {
    // 	fs: "empty"
    // }
};
